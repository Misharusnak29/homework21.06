/* Теоретичні питання 
1.Document Object Model (DOM) - це об'єктна модель, яка представляє структуру HTML або XML документа у вигляді дерева. В кожному вузлі дерева знаходиться окремий елемент, атрибут або текстовий вузол. DOM дозволяє програмам на JavaScript динамічно змінювати структуру, стиль і зміст документа. DOM є незалежним від мови програмування та дозволяє створювати, видаляти або змінювати елементи та їх атрибути в документі.

2.innerHTML повертає або задає HTML-код, який міститься всередині елемента. Це означає, що він включає в себе всі HTML-теги, які знаходяться всередині елемента.
innerText повертає або задає текстовий контент елемента, ігноруючи всі HTML-теги, та враховує відображення стилів (наприклад, приховані елементи не враховуються). Тобто, innerText відображає тільки видимий користувачу текст.

3.Є кілька способів звернення до елементів сторінки за допомогою JavaScript:

document.getElementById('id') - шукає елемент за унікальним ідентифікатором.
document.getElementsByClassName('className') - шукає елементи за класом.
document.getElementsByTagName('tagName') - шукає елементи за тегом.
document.querySelector('selector') - шукає перший елемент, що відповідає CSS-селектору.
document.querySelectorAll('selector') - шукає всі елементи, що відповідають CSS-селектору.

4.NodeList - це колекція вузлів (node) документа. Вона може бути статичною (коли зміни в DOM не відображаються) або динамічною (коли зміни в DOM автоматично оновлюють колекцію).
HTMLCollection - це колекція HTML-елементів. Вона завжди є "живою" (динамічною), тобто зміни в DOM автоматично відображаються в колекції.
*/

// Практичні завдання
// 1.

// Перший спосіб - використання getElementsByClassName
// var featuresByClassName = document.getElementsByClassName('feature');
// console.log(featuresByClassName);

// Другий спосіб - використання querySelectorAll
// var featuresByQuerySelector = document.querySelectorAll('.feature');
// console.log(featuresByQuerySelector);


// featuresByQuerySelector.forEach(function(feature) {
//   feature.style.textAlign = 'center';
// });


// 2.

// const awesomeFeature = document.querySelectorAll('.awesome-feature');

// awesomeFeature.forEach(function(h2) {
// h2.textContent = 'Awesome Feature'
// })

// 3.

// const awesomeTitle = document.querySelectorAll('.feature-title');

// awesomeTitle.forEach(function(h1) {
// h1.textContent += '!'
// })


